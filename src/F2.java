import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class F2 {
    static Scanner in = new Scanner(System.in);

    public static final int N = 250;
    public static final int K = 15;
    public static final int S = 11;

    public static int lower = 193;
    public static int upper = 196;

    public static int div1 = lower + (upper - lower) / 3;
    public static int div2 = lower + (upper - lower) / 3 * 2;
    public static int div3 = lower + (upper - lower) / 3 * 3;

    public static boolean solved = false;

    public static void printRequest() throws FileNotFoundException {
        PrintStream req1 = new PrintStream("req1.out");
        PrintStream req2 = new PrintStream("req2.out");

        System.out.println("Bucket 1 is [" + lower + ", " + div1 + "[");
        System.out.println("Bucket 2 is [" + div1 + ", " + div2 + "[");
        System.out.println("Bucket 3 is [" + div2 + ", " + div3 + "[");

        String firstReq = "";
        String secondReq = "";
        for (int i = 0; i < N; i++) {
            if (i < lower || i >= upper) {
                firstReq += '-';
                secondReq += '-';
            } else if (i < div1) {
                firstReq += 'L';
                secondReq += '-';
            } else if (i < div2) {
                firstReq += 'R';
                secondReq += 'L';
            } else if (i < div3) {
                firstReq += '-';
                secondReq += 'R';
            } else {
                firstReq += '-';
                secondReq += '-';
                System.out.println(i
                        + " is out! If all groups seem balanced, try this one");
            }
        }

        req1.println("W");
        req2.println("W");
        for (int i = 0; i < K; i++) {
            req1.println(firstReq);
            req2.println(secondReq);
        }
    }

    private static void receiveResponse() {
        String resp = in.nextLine();
        int[] freq = new int[3];
        for (int i = 0; i < K; i++) {
            int code = resp.charAt(i) == 'L' ? 0 : resp.charAt(i) == '=' ? 1
                    : 2;
            freq[code]++;
        }
        System.out.println(Arrays.toString(freq));

        String resp2 = in.nextLine();
        freq[0] = freq[1] = freq[2] = 0;
        for (int i = 0; i < K; i++) {
            int code = resp2.charAt(i) == 'L' ? 0 : resp2.charAt(i) == '=' ? 1
                    : 2;
            freq[code]++;
        }
        System.out.println(Arrays.toString(freq));
    }

    private static void printCheckRequest(int idx) throws FileNotFoundException {
        PrintStream req = new PrintStream("req.out");
        req.println("W");
        for (int i = 0; i < K; i++) {
            int randRight = new Random().nextInt(N);
            if (randRight == idx) {
                randRight++;
            }
            for (int j = 0; j < N; j++) {
                if (j == idx)
                    req.print('L');
                else if (j == randRight) {
                    req.print('R');
                } else {
                    req.print('-');
                }
            }
            req.println();
        }
    }

    private static void printGuess(int idx, char weight)
            throws FileNotFoundException {
        PrintStream req = new PrintStream("req.out");
        req.println("G");
        req.println((idx + 1) + " " + weight);
    }

    public static void main(String[] args) throws FileNotFoundException {
        // printCheckRequest(193);
        // printGuess(193, 'L');
        // System.exit(0);

        printRequest();
        System.out.println("Written request to file. Give me the response");
        receiveResponse();
    }
}
