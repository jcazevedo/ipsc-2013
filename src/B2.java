import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class B2 {
    static PrintStream out;
    
    static Map<Long, Long> dp = new HashMap<>();

    public static long boredom(long i) {
        if (i == 1)
            return 0;
        
        Long res = dp.get(i);
        if (res == null) {
            res = (i / 2) * (long) ((i + 1) / 2) + boredom(i / 2)
                    + boredom((i + 1) / 2);
            dp.put(i, res);
        }
        return res;
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner in = new Scanner(System.in);
        out = new PrintStream("b2.out");

        int t = in.nextInt();
        for (int k = 0; k < t; k++) {
            int value = in.nextInt();
            out.println(boredom(value));
        }
    }
}
