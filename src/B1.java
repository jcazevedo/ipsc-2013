import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

public class B1 {
    static PrintStream out;

    public static final int MAX_N = 1000000;

    static long[] dp = new long[MAX_N + 1];

    public static void boredomDp() {
        for (int i = 1; i <= MAX_N; i++) {
            if (i % 3 == 0)
                dp[i] = (i / 3) * (long) (2 * i / 3) + dp[i / 3]
                        + dp[2 * i / 3];
            else if (i % 2 == 0)
                dp[i] = (i / 2) * (long) (i / 2) + 2 * dp[i / 2];
            else
                dp[i] = (i - 1) + dp[i - 1];
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner in = new Scanner(System.in);
        out = new PrintStream("b1.out");

        boredomDp();

        int t = in.nextInt();
        for (int k = 0; k < t; k++) {
            int value = in.nextInt();
            out.println(dp[value]);
        }
    }
}
