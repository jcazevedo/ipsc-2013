#include <iostream>
#include <cstring>

using namespace std;

#define MOD 1000000009
#define MAX 100001

int T, N;
long long waysUp[MAX];
long long waysDown[MAX];

long long modAdd(long long a, long long b) {
    return (a + b) % MOD;
}

long long modMult(long long a, long long b) {
    return (a * b) % MOD;
}

void calcWaysUp() {
    memset(waysUp, 0, sizeof(waysUp));
    waysUp[0] = 1;
    for (int i = 1; i < MAX; i++)
        for (int j = 1; j <= 2; j++)
            if (i - j >= 0)
                waysUp[i] = modAdd(waysUp[i], waysUp[i - j]);
}

void calcWaysDown() {
    memset(waysDown, 0, sizeof(waysDown));
    waysDown[0] = 1;
    for (int i = 1; i < MAX; i++)
        for (int j = 1; j <= 4; j++)
            if (i - j >= 0)
                waysDown[i] = modAdd(waysDown[i], waysDown[i - j]);
}

int main() {
    calcWaysUp();
    calcWaysDown();

    cin >> T;
    for (int i = 0; i < T; i++) {
        cin >> N;
        cout << modMult(waysUp[N], waysDown[N]) << endl;
    }

    return 0;
}
