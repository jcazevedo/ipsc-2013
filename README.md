# Solutions to IPSC 2013 problems

This repository contains solutions to [Internet Problem Solving Contest (IPSC) 2013][1] problems. These solutions are provided "as is". I give no guarantees that they will work as expected.

## Problems status

Here is a list of the problems currently in this repository. Problems marked with ✓ are done, while problems with ✗ are not complete and/or aren't efficient enough.

* ✓ [A: Advice for Olivia][A] (`A.java`)
* ✓ [B: Boredom buster][B] (`B1.java`, `B2.java`)
* ✓ [F: Feeling lucky?][F] (`F1.java`, `F2.java`)
* ✓ [J: Just a single gate][J] (`J.cpp`)
* ✓ [K: Knee problems][K] (`K1.cpp`, `K2.cpp`)

[1]: http://ipsc.ksp.sk
[A]: http://ipsc.ksp.sk/2013/real/problems/a.html
[B]: http://ipsc.ksp.sk/2013/real/problems/b.html
[F]: http://ipsc.ksp.sk/2013/real/problems/f.html
[J]: http://ipsc.ksp.sk/2013/real/problems/j.html
[K]: http://ipsc.ksp.sk/2013/real/problems/k.html
